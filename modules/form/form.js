var $ = require('jquery');
var mask = require('jquery-mask-plugin');

module.exports = {
    forms: document.querySelectorAll('form'),
    submit () {
        console.log('hello');
    },
    init () {
        $('input[type=tel]').mask('+7 (000) 000-00-00');
        
        $('form').submit(function (e) {
            var that = $(this);
            $.ajax({
                type: that.attr('method'),
                url: that.attr('action'),
                data: that.serialize(),
                success: function (data) {
                    that.find('.form__content').html('<div class="form__title">Сообщение успешно отправленно</div>');
                    that.find('input').val('');
                },
                error: function (err) {
                    that.find('.form__note').html('Ошибка. Попробуйте еще раз.');
                }
            });
            e.preventDefault();
        });
    }
};