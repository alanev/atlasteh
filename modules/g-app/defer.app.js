var header = require('../header/header.js');
header.init();

require('../popup/popup.js');

// menu
var menu = require('../menu/menu.js');
menu.init();

// forms
var form = require('../form/form.js');
form.init();