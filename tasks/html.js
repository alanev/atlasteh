// paths
var paths = require('./paths');

// modules
var gulp = require('gulp'),
	beep = require('./beep'),
	plumber = require('gulp-plumber'),
    connect = require('gulp-connect'),
    prettify = require('gulp-prettify'),
    decode = require('ent/decode'),
	
	postxml = require('gulp-postxml'),
	plugins = [
		require('postxml-import')({
			path: function (attr) {
                if (!/(\\|\/|\.)/.test(attr)) {
                    return 'modules/' + attr + '/' + attr + '.htm';
                }
				return attr;
			}
		}),
		require('postxml-beml')(),
		require('postxml-imgalt')(),
		require('postxml-placeholder')({
			url: 'http://placehold.alanev.ru/'
		}),
		require('postxml-image-size')({
			cwd: paths.dest
		}),
		require('postxml-wrap')(),
		require('postxml-repeat')(),
        (function (options) {
            return function ($) {
                var decoded = decode($('html').html());
                $('html').html(decoded);
            }
        })()
	]
	;

// task
var task = function () {
	return gulp.src(paths.html.src)
		.pipe(plumber(beep))
		.pipe(postxml(plugins))
        .pipe(prettify({
            indent_char: '\t',
            indent_size: 1
        }))
		.pipe(plumber.stop())
		.pipe(gulp.dest(paths.html.dest))
		.pipe(connect.reload())
		;
}

// module
module.exports = task;