var header = {
    element: document.querySelector('.header'),
    activate () {
        this.element.classList.add('_active');
    },
    deactivate () {
        this.element.classList.remove('_active');
    },
    scroll () {
        if ( window.pageYOffset > 100) {
            header.activate();
        } else {
            header.deactivate();
        }
    },
    init: function () {
        window.addEventListener('scroll', this.scroll);
    }
};
module.exports = header;