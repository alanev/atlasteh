NodeList.prototype.map = Array.prototype.map;
var menu = {
    active: null,
    activeClass: '_active',
    sections: function () {
        return document.querySelectorAll('.g-section[id]').map(function (item) {
            return {
                top: item.offsetTop,
                bottom: item.offsetTop + item.offsetHeight
            };
        })
    },
    remove () {
        if (this.active !== null) {
            this.links[this.active].classList.remove(this.activeClass);
        }
    },
    add (num) {
        this.active = num;
        this.links[this.active].classList.add(this.activeClass);
    },
    links: document.querySelectorAll('.menu__item'),
    init () {
        var that = this;
        window.addEventListener('scroll', function () {
            var wCenter = window.scrollY + window.innerHeight / 2;
            
            that.sections().forEach(function (item, index) {
                if (wCenter > item.top && wCenter < item.bottom && index != that.index) {
                    that.remove();
                    that.add(index);
                }
            });
            
        });
    }
};

module.exports = menu;